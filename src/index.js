import React from 'react';
import ReactDOM from 'react-dom';
import { connect, Provider } from 'react-redux';
import { store } from './redux/store';
import { Player } from "./components/player/player";
import {mapStateToProps, mapDispatchToProps } from "./redux/map";
import { trackInfoContext } from "./context";
import { trackList } from "./trackList";

const WrappedPlayerComponent = connect(mapStateToProps("Player"), mapDispatchToProps("Player"))(Player);

ReactDOM.render(
    <Provider store={store}>
        <trackInfoContext.Provider value={ trackList }>
            <WrappedPlayerComponent/>
        </trackInfoContext.Provider>
    </Provider>,
    document.getElementById('root')
);

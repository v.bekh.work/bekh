import React from "react";
import {Playlist} from "../playlist/playlist";
import './player.css';
import {trackInfoContext} from "../../context";

export class Player extends React.Component {
    static contextType = trackInfoContext;

    constructor(props) {
        super(props);

        this.audioRef = React.createRef();
        this.timelineRef = React.createRef();
        this.bufferRef = React.createRef();
        this.currentTimeRef = React.createRef();
        this.durationTimeRef = React.createRef();
        
        this.state = {
            playState: 'pause',
            tracksOrder: null,
            repeatState: false
        }
    }

    render() {
        let currentTrack = this.getCurrentTrack();

        return (
            <div className="player">
                <Playlist
                    swapTrack={this.props.swapTrack}
                />
                <audio onEnded={this.repeatOrPlayNext} ref={this.audioRef} src={currentTrack.url} className="player"></audio>
                <div className="top">
                    <div className="inner">
                        <div className="controls">
                            <button
                                onClick={this.tooglePlayState.bind(this)}
                                className={this.getClassNameFromPlayState()}
                            >
                                play
                            </button>
                            <button className="previous" onClick={this.setPrevTrack}></button>
                            <button className="next" onClick={this.setNextTrack}></button>
                            <button className="shuffle" onClick={this.shuffleTracksOrder}></button>
                            <button className="repeat" onClick={this.toggleRepeatState.bind(this)}></button>
                        </div>
                        <div className="time current"><p ref={this.currentTimeRef}></p></div>
                        <div className="time duration"><p ref={this.durationTimeRef}></p></div>
                        <div className="info">
                            <p>
                                <span className="artist_name">{currentTrack.author} - </span>
                                <span className="song_name">{currentTrack.trackName}</span>
                            </p>
                        </div>
                        <div className="volume">
                            <input 
                                onInput={this.updateVolumeFromInput} 
                                value={this.props.volume} 
                                type="range" 
                                min="0" 
                                max="1" 
                                step="0.01" 
                                />
                        </div>
                    </div>
                </div>
                <div className="bottom">
                    <div className="inner">
                        <div className="timeline">
                            <div className="buffered" ref={this.bufferRef}>
                            </div>
                            <input
                                   ref={this.timelineRef}
                                   type="range"
                                   step="0.1"
                                   min="0"
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        this.timelineRef.current.value = this.getCurrentAudioTime();
        this.timelineRef.current.oninput = this.updateAudioTimeFromTimeLine;

        this.setCurrentTimeFromAudio();

        this.audioRef.current.onloadedmetadata = this.setSongDurationToHTML;
        this.audioRef.current.ontimeupdate = this.updateTimeLine;
    }
    
    setSongDurationToHTML = () => {
        let songDuration = this.getAudioDuration();

        this.timelineRef.current.max = songDuration;
        this.durationTimeRef.current.textContent = this.convertDurationToString(songDuration);
    }
    
    updateTimeLine = () => {
        this.setCurrentTimeFromAudio();
        this.timelineRef.current.value = this.getCurrentAudioTime();
        this.updateBufferWidth();
    }

    updateBufferWidth = () => {
        if (this.audioRef.current.buffered.length) {
            let load_percent = (this.getCurrentAudioTime() / 2) + 1;
            this.bufferRef.current.style.width = load_percent + "%";
        }
    }

    setCurrentTimeFromAudio = () => {
        let currentSongTime = this.getCurrentAudioTime();
        this.currentTimeRef.current.textContent = this.convertDurationToString(currentSongTime);
    }

    tooglePlayState() {
        this.audioRef.current.paused ? this.playAudio() : this.pauseAudio();
    }

    playAudio = () => {
        this.audioRef.current.play();
        this.setState({
            playState: 'play'
        });
    }

    pauseAudio = () => {
        this.audioRef.current.pause();
        this.setState({
            playState: 'pause'
        });
    }

    setNextTrack = () => {
        this.props.swapTrack( this.getNextTrackIndex() );
    }

    setPrevTrack = () => {
        this.props.swapTrack( this.getPrevTrackIndex() );
    }

    getNextTrackIndex = () => {
        let currentIndex = this.getCurrentTrackIndexInOrder();
        let nextIndex = currentIndex + 1;

        if( nextIndex >= this.getTracksCount() ) {
            nextIndex = 0;
        }
        
        return this.getTracksOrderItem(nextIndex);
    }

    getPrevTrackIndex = () => {
        let currentIndex = this.getCurrentTrackIndexInOrder();
        let prevIndex = currentIndex - 1;

        if( prevIndex < 0 ) {
            prevIndex = this.getTracksCount() - 1;
        }

        return this.getTracksOrderItem(prevIndex);
    }

    getCurrentTrackIndexInOrder = () => {
        let tracksOrder = this.getTracksOrder();
        let currentTrackIndexInString = String(this.props.currentTrackIndex);
        return tracksOrder.indexOf( currentTrackIndexInString );
    }

    repeatOrPlayNext = () => {
        if(this.state.repeatState) {
            this.audioRef.current.currentTime = 0;
            this.playAudio();
        } else {
            this.setNextTrack();
        }
    }

    toggleRepeatState() {
        this.setState({
            repeatState: !this.state.repeatState
        });
    }

    shuffleTracksOrder = () => {
        let currentOrder = this.getTracksOrder();
        for (let i = this.getTracksCount() - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1)); // случайный индекс от 0 до i
            [currentOrder[i], currentOrder[j]] = [currentOrder[j], currentOrder[i]];
        }
        this.setState({
            tracksOrder: currentOrder
        });
    }

    updateAudioTimeFromTimeLine = () => {
        this.updateAudioTime( this.timelineRef.current.value );
    }

    updateVolumeFromInput = (e) => {
        let newVolume = e.target.value;
        this.updateVolume(newVolume);
    }

    updateAudioTime = (newCurrentTime) => {
        this.audioRef.current.currentTime = newCurrentTime;
    }

    updateVolume = (newVolume) => {
        this.props.changeVolume(newVolume);
        this.audioRef.current.volume = newVolume;
    }

    convertDurationToString(seconds) {
        var h = Math.floor(seconds / 3600);
        var m = Math.floor(seconds % 3600 / 60);
        var s = Math.floor(seconds % 3600 % 60);

        var hDisplay = h > 0 ? h+":" : "";
        var mDisplay = m > 9 ? m : "0"+m;
        var sDisplay = s > 9 ? s : "0"+s;

        return hDisplay + mDisplay + ":" + sDisplay;
    };
    
    getClassNameFromPlayState() {
        switch (this.state.playState) {
            case 'play' : {
                return 'pause';
            }
            case 'pause' : {
                return 'play';
            }
            default:
                return 'play';
        }
    }

    getTracksOrderItem = (index) => {
        let order = this.getTracksOrder();
        return order[index];
    }

    getTracksOrder = () => {
        return this.state.tracksOrder || Object.keys(this.context);
    }

    getTracksCount = () => {
        return this.context.length;
    }

    getCurrentTrack = () => {
        return this.context[ this.props.currentTrackIndex ]
    }

    getCurrentAudioTime = () => {
        return this.audioRef.current.currentTime.toFixed(1);
    }

    getAudioDuration = () => {
        return this.audioRef.current.duration;
    }
}
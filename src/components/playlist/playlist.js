import React from "react";
import './playlist.css';
import { trackInfoContext } from "../../context";

export class Playlist extends React.Component {
    static contextType = trackInfoContext;
    constructor(props) {
        super(props);
    }

    swapTrack() {
        this.props.swapTrack(this.context[1]);
    }

    render() {
        return (
            <div className="swapTrack" onClick={this.swapTrack.bind(this)}>
            </div>
        );
    }
}



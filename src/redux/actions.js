export const changeVolume = (volume) => {
    return {
        type: 'CHANGE_VOLUME',
        payload: volume
    }
}

export const swapTrack = (trackIndex) => {
    return {
        type: 'SWAP_TRACK',
        payload: trackIndex,
    }
}
export const rootReducer = (state, action) => {
    switch (action.type) {
        case 'CHANGE_VOLUME': 
            return { 
                ...state, 
                volume: action.payload 
            }
        case 'SWAP_TRACK':
            return {
                ...state, 
                currentTrackIndex: action.payload 
            }
        default: return state
    }
};
import {bindActionCreators} from "redux";
import {changeVolume, swapTrack} from "./actions";

export const mapStateToProps = (component) => {
    switch(component) {
        case "Player": {
            return function (state) {
                return {
                    currentTrackIndex: state.currentTrackIndex,
                    volume: state.volume
                };
            }
        }
        default: return undefined;
    }
}

export const mapDispatchToProps = (component) => {
    switch(component) {
        case "Player": return function(dispatch) {
            return {
                changeVolume: bindActionCreators(changeVolume, dispatch),
                swapTrack: bindActionCreators(swapTrack, dispatch)
            };
        };
        default: return undefined;
    }
}